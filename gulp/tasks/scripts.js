
var gulp       = require('gulp'),
    concat     = require('gulp-concat'),
    uglify     = require('gulp-uglify'),
    gulpif     = require('gulp-if'),
    sourcemaps = require('gulp-sourcemaps'),
    plumber    = require('gulp-plumber'),
    main_bower = require('main-bower-files'),
    config     = require('../config').scripts;

gulp.task('scripts', function () {

  return (
    gulp.src( main_bower({filter: /.+\.js$/}).concat(config.src) )
        .pipe( plumber({
          errorHandler: function (error) {
            console.error('Error: ' + error.message);
            this.emit('end');
          }}) )
        .pipe( gulpif(devBuild, sourcemaps.init({loadMaps: true})) )
        .pipe( concat("app.js") )
        .pipe( gulpif(!devBuild, uglify()) )
        .pipe( gulpif(devBuild, sourcemaps.write('./')) )
        .pipe( gulp.dest(config.dest) )
  );

});

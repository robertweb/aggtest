
var gulp        = require('gulp'),
    gutil 		  = require('gulp-util'),
    fallback    = require('connect-history-api-fallback'),
    browserSync = require('browser-sync').get('dev'),
    config      = require('../config').server;

gulp.task('serve', ['build'], function () {

  var supportedEvents = ['add', 'change', 'unlink'];

  browserSync.init({
    server: {
      baseDir: config.src,
      directory: false,
      middleware: [ fallback() ]
    },
    open: true,
    host: config.host,
    port: config.port,
    reloadOnRestart: true,
    logLevel: config.log.logLevel,
    logPrefix: config.log.logPrefix,
    files: [
      {
        match: [config.src],
        options: {
          ignored: [
            /[\/\\]\./,
            '**/*.db',
            '**/*.css',
            '**/*.map',
            '**/*.tmp'
          ]
        }
      },
      {
        match: [config.watch],
        fn: function (event, file) {
          gutil.log('File has been ' + gutil.colors.cyan(event) + ': ', gutil.colors.magenta(file));
          if (supportedEvents.indexOf(event) > -1) {
            if (/\.js$/.test(file)) gulp.start('scripts');
            else if (/\.scss$/.test(file)) gulp.start('scss');
            else gulp.start('copy');
          }
        },
        options: {
          ignoreInitial: true,
          ignored: [
            /[\/\\]\./,
            /___$/,
            '**/*.db',
            '**/*.tmp'
          ]
        }
      }
    ]
  });

});

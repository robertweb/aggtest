"use strict";

var gulp        = require('gulp'),
    concat      = require('gulp-concat'),
    process     = require('gulp-sass'),
    prefix      = require('gulp-autoprefixer'),
    compress    = require('gulp-csso'),
    gulpif      = require('gulp-if'),
    plumber     = require('gulp-plumber'),
    sourcemaps  = require('gulp-sourcemaps'),
    browserSync = require('browser-sync').get('dev'),
    config      = require('../config').scss;

gulp.task('scss', function () {

  return (
    gulp.src(config.src)
      .pipe( plumber({
        errorHandler: function (error) {
          console.error('Error: ' + error.message);
          this.emit('end');
        }}) )
      .pipe( gulpif(devBuild, sourcemaps.init({loadMaps: true})) )
      .pipe( process() )
      .pipe( prefix(config.autoprefixer) )
      .pipe( gulpif(!devBuild, compress(config.compress)) )
      .pipe( gulpif(devBuild, sourcemaps.write('./')) )
      .pipe( gulp.dest(config.dest) )
      .pipe( browserSync.stream({match: '**/*.css'}) )
  );

});

var gulp   = require('gulp'),
    rename = require('gulp-rename'),
    config = require('../config').copy;

gulp.task('copy', function () {

  return (
    gulp.src(config.from, { base : config.base })
        .pipe(rename(function (path) {
          path.dirname = path.dirname.replace("app", "views");
        }))
        .pipe(gulp.dest(config.to))
  );

});


var _pkg    = require('../package.json');

/* Paths */

var _src    = './src/',
    _build  = './public/';

var _app    = 'app/',
    _js     = 'js/',
    _css    = 'css/';

/* Tasks params */

module.exports = {

  scripts: {
    src: [
      _src + _app + 'app.module.js',
      _src + _app + 'app.constants.js',
      _src + _app + 'app.router.js',
      _src + _app + '**/*.js'
    ],
    dest: _build + _js
  },

  scss: {
    src: [
      _src + _app + 'app.scss'
    ],
    dest: _build + _css,
    autoprefixer: {
      browsers: ['last 10 versions'],
      cascade: false
    },
    compress: {}
  },

  clear: {
    src: _build + '**/*'
  },

  copy: {
    base: _src,
    from: [
      _src + '**/*.html'
    ],
    to: _build
  },

  server: {
    src: _build,
    watch: _src,
    host: 'localhost',
    port: '8040',
    log: {
      logLevel: "info",
      logPrefix: _pkg.name + ' v' + _pkg.version
    }
  }

};

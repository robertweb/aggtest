+function () {
  'use strict';

  angular
      .module('app')
      .config(routerConfig);

  routerConfig.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider', 'ROUTING', 'VIEW'];
  
  function routerConfig($stateProvider, $urlRouterProvider, $locationProvider, ROUTING, VIEW) {

    var catalogState = {
      name: 'catalog',
      url: ROUTING.CATALOG,
      views: {
        "header": { templateUrl: VIEW.CATALOG_PATH + VIEW.CATALOG_HEADER },
        "content": {
          templateUrl: VIEW.CATALOG_PATH + VIEW.CATALOG_CONTENT,
          controller: 'CatalogController as catalog'
        }
      },
      resolve: {
        library: getCatalog
      }
    };

    var bookState = {
      name: "book",
      url: ROUTING.BOOK,
      views: {
        "header": {
          templateUrl: VIEW.BOOK_PATH + VIEW.BOOK_HEADER,
          controller: 'BookController as book'
        },
        "content": {
          templateUrl: VIEW.BOOK_PATH + VIEW.BOOK_CONTENT,
          controller: 'BookController as book'
        }
      },
      resolve: {
        item: getBookItem,
        bundles: getBookBundles
      }
    };
    
    var notFoundState = {
      name: 'notFound',
      url: ROUTING.NOT_FOUND,
      views: {
        "content": {
          templateUrl: VIEW.ERROR_PATH + VIEW.NOT_FOUND
        }
      }
    };

    var apiErrorState = {
      name: 'apiError',
      url: ROUTING.API_ERROR,
      views: {
        "content": {
          templateUrl: VIEW.ERROR_PATH + VIEW.API_ERROR
        }
      }
    };

    $urlRouterProvider
        .when('/', ROUTING.CATALOG)
        .otherwise(routerErrorHandler);
    $stateProvider
        .state(catalogState)
        .state(bookState)
        .state(notFoundState)
        .state(apiErrorState);
    $locationProvider.html5Mode({
      enabled: true,
      requireBase: false
    });

    getCatalog.$inject = ['$log', '$state', 'CatalogService'];
    getBookItem.$inject = ['$log', '$state', '$stateParams', 'BookService'];
    getBookBundles.$inject = ['$log', '$state', '$stateParams', 'BundleService'];

    function getCatalog($log, $state, CatalogService) {
      return CatalogService.query()
          .$promise.catch(apiErrorHandler);

      function apiErrorHandler(reason) {
        $log.warn("API returned an error - " + reason.data.error);
        $state.go("apiError");
      }
    }

    function getBookItem($log, $state, $stateParams, BookService) {
      return BookService.get( {bookId: $stateParams.id} )
          .$promise.catch(apiErrorHandler);

      function apiErrorHandler(reason) {
        $log.warn("API returned an error - " + reason.data.error);
        $state.go("apiError");
      }
    }

    function getBookBundles($log, $state, $stateParams, BundleService) {
      return BundleService.query( {bookId: $stateParams.id} )
          .$promise.catch(apiErrorHandler);

      function apiErrorHandler(reason) {
        $log.warn("API returned an error - " + reason.data.error);
        $state.go("apiError");
      }
    }

    function routerErrorHandler($injector) {
      $injector.invoke(["$log", "$state", errorHandler]);

      function errorHandler($log, $state) {
        $log.warn("State with current url not found");
        $state.go("notFound");
      }
    }
  }

}();
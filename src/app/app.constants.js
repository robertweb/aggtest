+function () {
  'use strict';

  angular
      .module('app')
      .constant('URL', {
        API: 'https://ds.aggregion.com/api',
        CATALOG: '/public/catalog',
        BOOK: '/public/catalog/:bookId',
        BUNDLES: '/public/catalog/:bookId/bundles',
        RESOURCE: 'https://storage.aggregion.com/api/files/',
        RESOURCE_SUFFIX: '/shared/data',
        RESOURCE_DEFAULT: '12ce171be47031a58f6d12ddefca93d52bda709b1b720d50cf48747d6cd44cb6'
      })
      .constant('ROUTING', {
        CATALOG: '/catalog',
        BOOK: '/catalog/:id',
        NOT_FOUND: '/404',
        API_ERROR: '/error'
      })
      .constant('VIEW', {
        CATALOG_PATH: '/views/catalog/',
        CATALOG_HEADER: 'catalog.toolbar.html',
        CATALOG_CONTENT: 'catalog.html',
        BOOK_PATH: '/views/book/',
        BOOK_HEADER: 'book.toolbar.html',
        BOOK_CONTENT: 'book.html',
        ERROR_PATH: '/views/error/',
        NOT_FOUND: '404.html',
        API_ERROR: 'error.html'
      });

}();
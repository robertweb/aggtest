+function () {
  'use strict';

  angular
      .module('app')
      .directive('rrFallBack', rrFallBack);

  rrFallBack.$inject = ['URL'];

  function rrFallBack(URL) {
    return {
      link: link,
      restrict: 'A'
    };
    
    function link(scope, element, attrs) {
      element.bind('error', function () {
        attrs.$set('src', URL.RESOURCE + URL.RESOURCE_DEFAULT + URL.RESOURCE_SUFFIX);
      });
    }
  }

}();
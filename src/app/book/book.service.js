+function () {
  'use strict';

  angular
      .module('app')
      .factory('BookService', BookService);

  BookService.$inject = ['$resource', 'URL'];

  function BookService($resource, URL) {
    return $resource(URL.API + URL.BOOK, { bookId: '@id' });
  }

}();
+function () {
  'use strict';

  angular
      .module('app')
      .controller('BookController', BookController);

  BookController.$inject = ['item', 'bundles', 'URL'];
  
  function BookController(item, bundles, URL) {
    var book = this;
    book.item = item;
    book.bundles = bundles;
    book.getResourceUrl = getResourceUrl;

    function getResourceUrl(hash) {
      return URL.RESOURCE + hash + URL.RESOURCE_SUFFIX;
    }
  }

}();
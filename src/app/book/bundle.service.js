+function () {
  'use strict';

  angular
      .module('app')
      .factory('BundleService', BundleService);

  BundleService.$inject = ['$resource', 'URL'];

  function BundleService($resource, URL) {
    return $resource(URL.API + URL.BUNDLES, { bookId: '@id' });
  }

}();
+function () {
  'use strict';

  angular
      .module('app')
      .factory('CatalogService', CatalogService);

  CatalogService.$inject = ['$resource', 'URL'];

  function CatalogService($resource, URL) {
    return $resource(URL.API + URL.CATALOG);
  }

}();
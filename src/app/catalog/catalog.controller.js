+function () {
  'use strict';

  angular
      .module('app')
      .controller('CatalogController', CatalogController);
  
  CatalogController.$inject = ['library', 'URL'];

  function CatalogController(library, URL) {
    var catalog = this;
    catalog.tiles = library;
    catalog.getResourceUrl = getResourceUrl;

    function getResourceUrl(hash) {
      return URL.RESOURCE + hash + URL.RESOURCE_SUFFIX;
    }
  }

}();
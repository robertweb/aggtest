
var gutil 		  = require('gulp-util'),
		requireDir 	= require('require-dir'),
		browserSync = require('browser-sync');

browserSync.create('dev');
global.devBuild = gutil.env.env !== 'production';
requireDir('./gulp/tasks', { recurse: true });